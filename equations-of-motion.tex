\documentclass[main.tex]{subfiles}

\begin{document}

\chapter{The Equations of Motion}
\label{chap:eom}

\small{
    This chapter corresponds roughly in content to Chapter 1 of Mechanics \cite{mechanics}, but does not follow the sections exactly, as it incorporates some material from my PHY354 notes and other sources.
}

\vspace{5mm}

These notes focus on mechanical systems, that is, systems at a macroscopic scale at which quantum and relativistic effects may be ignored, which are primarily under the influence of simple mechanical forces such as gravity and friction. Examples abound in everyday life, from pendulums to spinning tops to planets in orbit (barring, e.g., Mercury's slight precession due to relativistic effects). The simplest example of a mechanical system, however, is the slightly abstract notion of a particle. A particle is defined as a body whose extent may be neglected in describing its motion. As such, its state is fully described by its position in 3-dimensional space. We may represent this position as, e.g., 3 Cartesian coordinates \(x, y, z\), or, for example, in spherical coordinates \(r, \theta, \varphi\), but the minimum amount of information to (smoothly) do so is 3 real numbers. Of course, what exactly counts as a particle depends somewhat on context: the Earth may be treated as a particle when studying its orbit around the Sun, but definitely not at smaller scales!

In general, the number of real-valued variables required to fully describe the state of a mechanical system at a given time is called its \nterm{degrees of freedom}. Any \(s\) variables \(\mv{q} = (q_1,...,q_s)\) which do so are called \nterm{generalized coordinates} for the system, and their time derivatives \(\dot{\mv{q}}= (\dot{q_1},...,\dot{q_s})\) are called \nterm{generalized velocities}. For example, both \((x, y, z)\) and \((r, \theta, \phi)\) are generalized coordinates for a particle in 3-dimensional space. In general, it is not sufficient to know just the generalized coordinates of a system at a given point in time to determine its future evolution. For example, consider a basketball. The basketball may begin at exactly the same point in space, but whether it's velocity is to the left or to the right will drastically affect its future trajectory. On the other hand, it is known from experience that knowing both the generalized coordinates and velocities of a mechanical system is enough to determine its future states.

An alternative way of saying this is that, given coordinates \(\mv{q}\) and velocities \(\dot{\mv{q}}\), we may uniquely determine the \nterm{generalized accelerations} \(\ddot{\mv{q}}\), which is equivalent by the elementary properties of ordinary differential equations to saying that the future trajectory is determined. It is in this sense that Newtonian mechanics are said to be \nterm{second order}. In other words, we may find a function \(f\) such that
\begin{equation}
    \ddot{\mv{q}} = f(\mv{q}, \dot{\mv{q}})
    \label{eqn:eom-are-second-order}
\end{equation}
The above equation, which may be written into multiple equations (e.g. one for each coordinate), is what is referred to as the \nterm{equations of motion} for the mechanical system in question. Integrating these equations yields a function \(q(t)\) determining the system's position at all future instants in time.

We will, hence, face three main problems in our study of mechanical systems. Firstly, we will often have to figure out how to effectively parametrize systems, preferably in a manner that is both elegant and easy to work with. Secondly, we will have to figure out how to derive the equations of motion for our description, again, preferably in a manner that is both elegant and easy to work with. The final issue we face is that integrating the resulting equations is often, in practice, intractable. Hence, we are faced with the problem of either coming up with clever tricks to obtain an exact solution (perhaps by fiddling with the parametrization and associated equations of motion) or coming up with approximate solutions or other descriptions of and constraints on behavior (such as e.g., numerical simulations). This is where much of the bulk of the work will be, but is what makes physics more than merely a branch of mathematics, though the properties of such differential equations, of course, are fascinating even from a purely mathematical standpoint, especially as they illustrate, e.g., chaos theory. We will, however, begin with the second step. In particular, we will demonstrate the derivation of the equations of motion of a given system parametrization from first principles, namely, the "Principle of Least Action," or Hamilton's Principle.

\section{The Principle of Least Action}
\label{sec:least-action}

The most general formulation of the laws governing mechanical systems is the Principle of Least Action, also known as Hamilton's Principle. In this formalism, a system with \(s\) generalized coordinates \(\mv{q} = (q_1,...,q_s)\) is described by a function
\begin{equation}
    \mc{L}(\mv{q}, \dot{\mv{q}}. t)
    = \mc{L}(q_1,...,q_s,\dot{q_1},...,\dot{q_s}, t)
\end{equation}
which we call the \nterm{Lagrangian} of the system. Assume now that the state of the system at instants \(t_1, t_2\) is given by vectors \(\mv{q}^{(1)}, \mv{q}^{(2)}\). Then, the principle of least action states that the system evolves along a path such that the quantity
\begin{equation}
    S = S[\mv{q}] = \int_{t_1}^{t_2}\mc{L}(\mv{q}, \dot{\mv{q}}, t)dt
    \label{eqn:define-action}
\end{equation}
is minimized. We call \(S\) the \nterm{action}. Note that in the above definition, \(\mv{q}\) is taken as a function of time \(\mv{q}(t)\) describing the state of the system through time, i.e. the ``path" the system takes through ``state space." We will now attempt to derive the differential equations which this implies the state of the system must obey; as \(\mc{L}\) depends only on \(\mv{q}\) and \(\dot{\mv{q}}\), we expect these to be second-order and hence for Equation \ref{eqn:eom-are-second-order} to hold. For simplicity, we will only consider the case of a single generalized coordinate \(q \in \reals\), but the proof generalizes readily to the multivariable case \(\mv{q} \in \reals^s\) with only a little extra bookkeeping.

In particular, let \(q = q(t)\) be a function for which \(S\) is minimized and for which the boundary conditions are satisfied, i.e. \(q(t_i) = q^{(i)}\) for \(i \in \{1, 2\}\). In particular, this means that for a small perturbation \(\delta q(t)\), we have, where \(q' = q + \delta q\) is another candidate solution,
\begin{equation}
    S[q'] \geq S[q]
\end{equation}
Since we require \(q'\) to satisfy the boundary conditions as well, we have, again for \(i \in \{1, 2\}\),
\begin{equation}
    q'(t_i) = q(t_i) = q^{(i)} \implies \delta q(t_i) = 0
    \label{eqn:perturb-end-is-zero}
\end{equation}
We furthermore have, by definition, that
\begin{equation}
    \delta S = S[q'] - S[q] = \int_{t_1}^{t_2}\mc{L}(q + \delta q, \dot{q} + \dot{\delta q}, t)dt - \int_{t_1}^{t_2}\mc{L}(q, \dot{q}, t)dt
\end{equation}
Performing a Taylor expansion of the first term with respect to \(\delta q\), we obtain
\begin{equation}
    \mc{L}(q + \delta q, \dot{q} + \dot{\delta q}, t) =
    \mc{L}(q, \dot{q}, t)
    + \prt{\mc{L}(q ,\dot{q}, t)}{q}\delta q
    + \prt{\mc{L}(q, \dot{q}, t)}{\dot{q}}\dot{\delta q}
    + \mc{O}(\delta q^2)
\end{equation}
implying, in particular, that
\begin{equation}
    \delta S = \int_{t_1}^{t_2}\left(
    \prt{L}{q}\delta q + \prt{L}{\dot{q}}\dot{\delta q} + \mc{O}(\delta q^2)
    \right)dt
    \label{eqn:taylor-action}
\end{equation}
It is a necessary condition for \(S\) to be a minimum (or, indeed, in general, an extremum) that
\(\delta S \in \mc{O}(\delta q^2)\)
Since the only first-order terms are those in Equation \ref{eqn:taylor-action}, this reduces to the condition that
\begin{equation}
    \int_{t_1}^{t_2}\left(
    \prt{L}{q}\delta q + \prt{L}{\dot{q}}\dot{\delta q}
    \right)dt = \int_{t_1}^{t_2}\prt{L}{q}\delta qdt + \int_{t_1}^{t_2}\prt{L}{\dot{q}}\dot{\delta q}dt = 0
    \label{eqn:extrema-condition-lagrange}
\end{equation}
As we have by definition that \(\dot{\delta q} = \drv{\delta q}{t}\), we may apply integration by parts to obtain
\begin{equation}
    \int_{t_1}^{t_2}\prt{L}{\dot{q}}\dot{\delta q}dt
    = \int_{t_1}^{t_2}\prt{L}{\dot{q}}d\delta q
    = \cancel{\left.\prt{\mc{L}}{\dot{q}}\delta q\right|_{t_1}^{t_2}}
    - \int_{t_1}^{t_2}\delta q d\left(\prt{\mc{L}}{\dot{q}}\right)
    = -\int_{t_1}^{t_2}\drv{}{t}\left[
        \prt{\mc{L}}{\dot{q}}
        \right]\delta qdt
\end{equation}
Note that it is Equation \ref{eqn:perturb-end-is-zero} which allows us to cancel \(\left.\prt{\mc{L}}{\dot{q}}\delta q\right|_{t_1}^{t_2} = 0\). Substituting this into Equation \ref{eqn:extrema-condition-lagrange}, we obtain the condition
\begin{equation}
    \int_{t_1}^{t_2}\prt{L}{q}\delta qdt
    - \int_{t_1}^{t_2}\drv{}{t}\left[
        \prt{\mc{L}}{\dot{q}}
        \right]\delta qdt
    = \int_{t_1}^{t_2}\left(
    \prt{L}{q}
    - \drv{}{t}\left[
        \prt{\mc{L}}{\dot{q}}
        \right]
    \right)\delta qdt = 0
    \label{eqn:lagrange-source}
\end{equation}
To proceed, we may apply the Main Theorem of Variational Calculus, which we state and provide an informal proof of below:
\begin{theorem}[Main Theorem of Variational Calculus]
    Given \(f\), if for all (compactly supported, smooth) \(\delta q\),
    \begin{equation}
        \int_{t_1}^{t_2}f(t)\delta q(t)dt = 0
    \end{equation}
    then we must have \(f\) is uniformly zero on \((t_1, t_2)\)
\end{theorem}
\begin{proof} (Sketch)
    Fix \(t^* \in (t_1, t_2\) and let \(\delta_n\), for \(n \in \nats\), be a series of smooth humps centered at 0 approaching the Dirac delta function. Then
    \begin{equation}
        \lim_{n \to \infty}\int_{t_1}^{t_2}\delta_n(t - t^*)f(t)
        = f(t^*) = \lim_{n \to \infty}0 = 0
    \end{equation}
\end{proof}
Applying this to Equation \ref{eqn:lagrange-source}, we obtain
\begin{equation}
    \prt{L}{q}
    - \drv{}{t}\left[
        \prt{\mc{L}}{\dot{q}}
        \right] = 0
    \iff \prt{L}{q} = \drv{}{t}\left[
        \prt{\mc{L}}{\dot{q}}
        \right]
    \label{eqn:euler-lagrange}
\end{equation}
i.e. \nterm{Lagrange's equations}, or the \nterm{Euler-Lagrange Equations}. To show the correspondence between this formulation and classical mechanics, let \(\mv{q}\) be the generalized coordinates for some system of particles. If \(T, U\) denote the kinetic and potential energy of the system, respectively, then setting
\begin{equation}
    \mc{L} = T - U = \frac{m\dot{\mv{q}}^2}{2} - U(\mv{q})
\end{equation}
in Equation \ref{eqn:euler-lagrange} yields
\begin{equation}
    \prt{L}{q} = m\ddot{q} = -\prt{U}{q} = -U' \implies m\ddot{q} = -U'
\end{equation}
which is precisely Newton's classical equations of motion,
\begin{equation}
    F = m\ddot{x} = -U'(x)
\end{equation}
The derivation of \ref{eqn:euler-lagrange} holds almost exactly for the cast of \(\mv{q} = (q_1,...,q_s)\) generalized coordinates, save that we must introduce additional bookkeeping so as to vary each individual coordinate. In this case, our result gives us \(s\) differential equations
\begin{equation}
    \forall i \in \{1,...,s\}, \quad
    \prt{L}{q_i}
    - \drv{}{t}\left[
        \prt{\mc{L}}{\dot{q_i}}
        \right] = 0
    \iff \prt{L}{q_i} = \drv{}{t}\left[
        \prt{\mc{L}}{\dot{q_i}}
        \right]
\end{equation}

\section{The Classical Limit}

\TODO{Consult treatment of this material in Woit \cite{woit}, in particular \(\hbar \to 0\)}

We will now, briefly, explore the connection between the least-action formalism above and quantum mechanics. This section is based primarily on an excellent lecture by Professor David Curtin for PHY354, given on January 10, 2018, at the University of Toronto, a partial transcript of which is at \url{https://gitlab.com/tekne/PHY354_Notes/-/blob/master/january10.pdf}.

Many years ago, upon learning about the least action principle, Professor Curtin's initial reaction was one of surprise: ``Huh? How does the particle know to minimize the action?'' We cannot understand why within the framework of classical mechanics. However, it turns out the least action principle provides a 19th-century clue to quantum mechanics. Let us consider why:

We understand today that classical mechanics is only a \emph{limit} of QM. Formally speaking, the behavior described by quantum mechanics approaches that described by classical mechanics as the Planck's constant, \(\hbar = \frac{h}{2\pi} \approx 10^{-34} J \cdot s\), approaches zero. What we mean, of course, by saying that a fundamental constant ``approaches zero" is that it does so relative to our scale of measurement (having units of energy \(\times\) time), or more precisely, that the ratio
\begin{equation}
    \frac{S}{\hbar} \to \infty
\end{equation}
where \(S\) is the action from Equation \ref{eqn:define-action}. For example, take the hydrogen atom. We know that the speed of the electron in a bound state is roughly
\begin{equation}
    v = \alpha c, \qquad
    \alpha = \frac{e^2}{4\pi\epsilon_0\hbar c} \approx \frac{1}{132}
\end{equation}
The Bohr radius us
\begin{equation}
    a_0 = \frac{\hbar}{mc\alpha} \approx \frac{1}{2} \cdot 10^{-9}m, \qquad
    m_e \approx 10^{-30} kg, \qquad
    m_ec^2 = 511 keV
\end{equation}
Hence, we have action
\begin{equation}
    S \approx \text{kinetic energy} \cdot \text{time}
    \approx \int_0^T\frac{mv^2}{2}dt
    \approx T\frac{m_ev^2}{2}
    \approx \frac{m_ev^2}{2}\frac{2\pi a_0}{v}
    \approx \pi m_eva_0
\end{equation}
Quantum mechanics tells us that angumar momentum \(\mb{r} \times \mb{p}\) is quantized, so \(m_eva_0 \approx \hbar\). So
\begin{equation}
    \frac{S}{\hbar} \approx 1 \implies \text{The hydrogen atom system is quantum}
\end{equation}
On the other hand, for the Earth and the Sun,
\begin{equation}
    \frac{S}{\hbar} \approx 10^{75} \approx \infty \implies \text{The Earth-sun system is classical}
\end{equation}

\begin{figure}
    \centering
    \input{figures/double-slit.tex}
    \caption{
        A simple diagram of the famous double-slit experiment: electrons, starting at point 1, and travel through two small slits to a phosphor-coated screen. In practice, the electrons make a diffraction pattern (if the slits are small enough) and yet hit the back of the apparatus as individual particles, illustrating particle-wave duality.
    }
    \label{fig:double-slit}
\end{figure}

Now what does this have to do with the Least Action Principle? In Quantum Mechanics, we say that the particle ``takes all possible paths," as in the double slit experiment in Figure \ref{fig:double-slit}, where the electron diffracts. The reason it is probably that the electron will land at point 2 and not at point 3 (in Figure \ref{fig:double-slit}) is that the wave function \(\phi\) of the electron undergoes constructive interference, so \(|\phi|^2 \approx \text{probability}\) is large. Feynman formulated quantum mechanics in terms of the Feynman path integral, roughly,
\begin{equation}
    A = |\phi|^2\alpha\sum_{\text{all paths from }1\to2}\exp\left(\frac{i}{\hbar}S_{path}\right)
\end{equation}
Now, take the classical limit as \(\hbar \to 0\). If we then choose a random path \(q(t)\), all it's ``neighboring paths" (infinitesimally) interfere destructively, implying \(A \to 0\). But if we take the path \(q(t)\) which extremizes \(S[q]\), then
\begin{equation}
    \frac{\delta S}{\delta \alpha} = 0
\end{equation}
so all neighboring paths have ``the same action," implying constructive interference. In the classical limit, then, the quantum mechanical path integral is \emph{dominated} by the path which has the least action, so Hamilton's principle becomes a better and better approximation.

In general, one of the key reasons the least action principle is useful in physics is that it allows us to formulate problems in a co-ordinate independent manner. Moreover, it also lets us do so on curved spaces (i.e., manifolds) and constrained systems. Hence, it is essential for understanding quantum mechanics, general relativity, and classical electrodynamics and is the primary tool in elementary particle physics, quantum field theory, and string theory.

\section{Properties of the Lagrangian}

We will now briefly digress to prove some mathematical properties of the Lagrangian, which will come in handy in the next section when we start to do actual physics. In particular, we will examine some of the consequences of the linearity of the integral defining the action, followed by some more general algebraic manipulations.

We begin by noting that, for any Lagrangian \(\mc{L}\), if a path \(q\) minimizes the action
\begin{equation}
    S[q] = \int_{t_1}^{t_2}\mc{L}(q, \dot{q}, t)dt
\end{equation}
then it must naturally minimize the action for \(\mc{L}' = \alpha\mc{L}\) for any \(\alpha \in \reals^+\), since by the linearity of the integral
\begin{equation}
    S'[q] = \int_{t_1}^{t_2}\mc{L}'(q, \dot{q}, t)dt
    =  \int_{t_1}^{t_2}\alpha\mc{L}(q, \dot{q}, t)dt
    = \alpha\int_{t_1}^{t_2}\alpha\mc{L}(q, \dot{q}, t)dt = \alpha S[q]
\end{equation}
Hence, we may multiply the Lagrangian by any constant without changing the system's mechanical behaviour, or alternatively, the Lagrangian is only defined up to a multiplicative constant (much like, e.g., an integral is only defined up to an additive constant).

We now consider the case of two mechanical systems \(A\) and \(B\), say, for example, two clusters of particles or a planet and some complex pulley system orbiting it. We now consider the compound mechanical system \(C = A \otimes B\) composed of \(A\), \(B\), and the interactions between them, treating it for now as (approximately) closed. Assume first that \(A\) and \(B\) are completely isolated, i.e., each behaves as if it is a closed system. Let \(a = (a_1,...,a_n)\) and \(b = (b_1,...,b_m)\) be generalized coordinates for \(A\) and \(B\), such that they have Lagrangians \(\mc{L}_A\) and \(\mc{L}_B\) respectively.

We note that the action behaves linearly on components of the Lagrangian, that is, by the linearity of the integral,
\begin{equation}
    \int_{t_1}^{t_2}[\mc{L}(q, \dot{q}, t) + \mc{L}'(q, \dot{q}, t)]dt = \int_{t_1}^{t_2}\mc{L}(q, \dot{q}, t)dt + \int_{t_1}^{t_2}\mc{L}'(q, \dot{q}, t)dt
\end{equation}
As we are treating \(A\), \(B\) as if they were closed, it makes sense to assume that the trajectories \(a(t)\), \(b(t)\) will minimize the actions \(S_A\), \(S_B\) of each system. Hence, if we take the Lagrangian of the overall system to be simply
\begin{equation}
    \mc{L} = \mc{L}_A + \mc{L}_B
\end{equation}
we see that the expected trajectory \(q(t) = (a(t), b(t))\) minimizes the action
\begin{equation}
    S_A[q] + S_B[q] = S_A[a] + S_B[b]
\end{equation}
We may hence, by this informal argument, surmise that in the limit, as \(A\) and \(B\) increase in distance from each other, and hence their interaction weakens,
\begin{equation}
    \lim\mc{L} = \mc{L}_A + \mc{L}_B
\end{equation}

Finally, let \(\mc{L}\) be the Lagrangian for a system, and \(f(q, t)\) be an arbitrary mathematical function. Then, we have that, if we consider the Lagrangian
\begin{equation}
    \mc{L}'(q, \dot{q}, t) = \mc{L}(q, \dot{q}, t) + \drv{}{t}f(q, t)
\end{equation}
the action \(S'[q]\) of any path \(q\) under this Lagrangian is given by
\begin{equation}
    S'[q] = \int_{t_1}^{t_2}\mc{L}'(q, \dot{q}, t)dt
    = \int_{t_1}^{t_2}\mc{L}(q, \dot{q}, t)dt + \int_{t_1}^{t_2}\frac{df}{dt}dt
    = S[q] + f(q^{(2)}, t_1) - f(q^{(1)}, t_1)
\end{equation}
Since the right-hand-side of this formula is constant for any paths \(q\) satisfying the boundary conditions, it follows that \(S[q]\) is minimized if and only if \(S'[q]\) is. In other words, \(\mc{L}'\) is also a valid Lagrangian for the original system we were given, i.e., the Lagrangian is only defined within an additive total time derivative of an arbitrary function of co-ordinates and time.

\section{The Assumptions of Classical Mechanics}

To recap, we've shown that minimizing the action yields the Euler-Lagrange equations and hinted at the connection between the least action principle and quantum mechanics. We've then explored some basic properties this implies about physical systems. We now want to figure out the most general form of the Lagrangian \(\mc{L}\) for an arbitrary number \(N\) of (classical) particles. To do so, we will proceed from first principles, using the basic assumptions of classical mechanics. Of course, these assumptions were from before Einstein and quantum mechanics. However, part of our goal is to understand and appreciate the historical development of theoretical physics. This perspective allows us to, in a sense, retrace it with the benefit of hindsight.

In classical mechanics, we assume an absolute space \(\reals^3\), points in which are given by vectors \(\mb{r} = (x, y, z)\). There is an absolute time that is homogeneous (the same everywhere), and hence a point in space at a given time may be represented as an element \((\mb{r}, t) \in \reals^3 \times \reals\). We may then define an inertial frame of reference to be a frame moving in uniform motion relative to absolute space at some velocity \(v \in (0, \infty)\) - with no upper limit. Note that we may take the Earth's surface to be roughly inertial if gravity is not important, for example, when considering billiards on the surface of a table.

With this definition in hand, we may state one of the most fundamental assumptions of classical mechanics: Galileo's relativity principle. Galileo's relativity principle says that any two inertial frames are mechanically equivalent, or in other words, ``physics is the same in all inertial frames." Hence, we may conclude that space is both \nterm{homogeneous} (since the laws of physics do not care where the origin is) and \nterm{isotropic} (since the laws of physics do not care about our orientation). These assumptions, it turns out, are enough to derive the Lagrangian \(\mc{L}\) for a single, ``free" particle.

Interestingly, challenging these spatial assumptions leads directly to relativistic mechanics, highlighting how relativity can be seen as an ``direct generalization'' of classical mechanics. For example, treating space as having Minkowski geometry \(\reals^{3, 1}\), where velocities add as if in a hyperbolic sphere of radius \(c\) (the speed of light), gives special relativity. In general relativity, we go further by giving spacetime the geometry of a 4-dimensional manifold; matter curves spacetime, and the curvature of spacetime tells matter how to move. Taking this further, we come to theories of quantum gravity (string theory), in which spacetime is emergent. We will now attempt to demonstrate how simple Lagrangians can be determined using only the assumptions of classical mechanics. Keep in mind that many of these alternative formalisms may \textit{also} be represented using Lagrangian mechanics, with different assumptions simply leading to different kinds of Lagrangian.

\section{Free Particles}

We proceed as follows: our particle's state, as described previously, can be taken to be a vector \(\mb{r} = (x, y, z) \in \reals^3\). We then say the particle has velocity \(\dot{\mb{r}} = \mb{v}\). We then require a Lagrangian of the form \(\mc{L}(\mv{q}, \dot{\mv{q}}, t) = \mc{L}(\mb{r}, \mb{v}, t)\). As time is homogeneous, we want the Lagrangian to be invariant under a linear time shift \(t' = t + c\). It follows that \(\mc{L}\) must be constant with respect to time, and hence can be written as a function of the form \(\mc{L}(\mb{r}, \mb{v})\). As space is also homogeneous, the Lagrangian should also be invariant under a linear shift \(\mb{r}' = \mb{r} + \mb{c}\), and hence, must be constant with respect to position, i.e., is a function of the form \(\mc{L}(\mb{v}\). As space is isotropic, we also want the equations of motion, and hence the Lagrangian, to be invariant under any rotation \(R\), i.e., we want
\begin{equation}
    \mc{L}(\mb{v}) = \mc{L}(R\mb{v})
\end{equation} Hence, we may write \(\mc{L}\) as simply a function \(\mc{L}(v)\), where
\begin{equation}
    v = |\mb{v}| = \sqrt{\dot{x}^2 + \dot{y}^2 + \dot{z}^2}
\end{equation}
is precisely the quantity preserved all rotations \(R\). As the Lagrangian is independent of \(\mb{r}\), we have
\begin{equation}
    \prt{\mc{L}}{\mb{r}}
    = \drv{}{t}\left(
    \prt{\mc{L}}{\mb{v}}
    \right) = 0
\end{equation}
As \(\mc{L}\) only depends on \(\mb{v}\), it follows that \(\mb{v}\) is a constant. We may hence conclude that free motion in an inertial frame takes place with velocity which is constant both in magnitude and direction, that is, Newton's first law of motion, or the \textit{law of inertia}.

\begin{theorem}
    \(\mc{L}\) must be of the form \(\mc{L}(v) = \frac{1}{2}mv^2\), for some constant \(m\).
    \label{thm:masstheorem}
\end{theorem}

\begin{proof}
    Given a system with velocity \(\mb{v}\), consider a ``slow" inertial frame with small velocity (with respect to the original frame)
    \(\bm{\epsilon}\). In this new frame, we have velocity
    \begin{equation}
        \mb{v}' = \mb{v} - \bm{\bm{\epsilon}}
    \end{equation}
    and hence Lagrangian
    \begin{equation}
        \mc{L}'(\mb{v}^2) = \mc{L}(\mb{v}'^2) = \mc{L}([\mb{v} - \bm{\epsilon}]^2)
    \end{equation}
    By Galileo's relativity principle, \(\mc{L}'(\mb{v})\) and \(\mc{L}(\mb{v})\) should describe the same system dynamics. Hence, they must differ by the total time derivative of a function \(f(\mb{r}, t)\) of the position and time. That is,
    \begin{equation}
        \mc{L}'(\mb{v}^2) = \mc{L}(\mb{v}^2) + \drv{}{t}f(\mb{r}, t)
    \end{equation}
    We perform Taylor expansion
    \begin{equation}
        \mc{L}'(\mb{v}) = \mc{L}([\mb{v} - \bm{\epsilon}]^2)
        = \mc{L}(\mb{v}^2 - 2\mb{v}\cdot\bm{\epsilon} + \bm{\epsilon}^2\mb{v}_1)
        = \mc{L}(\mb{v}^2) - 2\mb{v}\cdot\bm{\epsilon}\prt{\mc{L}}{\mb{v}^2} + \mc{O}(\bm{\epsilon}^2)
    \end{equation}
    Ignoring terms above the first order (in \(\bm{\epsilon}\)), i.e. taking \(\bm{\epsilon}\) to be infinitesimal, we obtain
    \begin{equation}
        \mc{L}'(\mb{v})
        = \mc{L}(\mb{v}^2) - 2\mb{v}\cdot\bm{\epsilon}\prt{\mc{L}}{\mb{v}^2}
        = \mc{L}(\mb{v}^2) + \drv{}{t}f(\mb{r}, t)
        \iff \drv{}{t}f(\mb{r}, t) = - 2\mb{v}\cdot\bm{\epsilon}\prt{\mc{L}}{\mb{v}^2}
    \end{equation}
    We have
    \begin{equation}
        \drv{}{t}f(\mb{r}(t), t) = \mb{v} \cdot \prt{f}{\mb{r}} + \prt{f}{t} =  - 2\mb{v}\cdot\bm{\epsilon}\prt{\mc{L}}{\mb{v}_1^2}
    \end{equation}
    Hence, we have \(\prt{f}{t} = 0\), so
    \begin{equation}
        \prt{f}{\mb{r}} = -2\bm{\epsilon}\prt{\mc{L}}{\mb{v}_1^2}
    \end{equation}
    Hence, as \(f\) (and therefore \(\prt{f}{\mb{r}}\)) is a function of only \(\mb{r}\) and \(t\), it follows that \(\prt{\mc{L}}{\mb{v}^2}\) must be independent of \(\mb{v}\), and hence that \(\mc{L}\) must be linear in \(\mb{v}^2\). As \(\mc{L}\) is \textit{only} allowed to depend on \(\mb{v}^2 = v^2\), it follows that it must be of the form \(\mc{L} = \frac{1}{2}mv^2\) (ignoring constants and total time-derivatives), as desired.
\end{proof}
We note that the quantity \(m\) cannot be negative, as if it was, \(\mc{L}\) would be unbounded below, implying the action would not have a minimum.
Naturally, we call the quantity \(m\) in Theorem \ref{thm:masstheorem} the \textbf{mass} of the particle.

The additivity of the Lagrangian allows us to generalize this directly to the case of \(N\) non-interacting particles, in which case we obtain the familiar formula
\begin{equation}
    \mc{L} = \frac{1}{2}\sum_im_iv_i^2
    \label{eqn:freelagrange}
\end{equation}
Note how, so far, we have managed to deduce all of these facts, going all the way as to derive mass, using nothing but \textit{symmetry}; this is going to be a recurring theme.

\section{Systems of Particles}

\TODO{Notation}

So far, we've only applied or formalism to the somewhat boring case of tiny particles floating, undisturbed, through empty space. We now consider how to generalize this to the more interesting situation of a \textit{system} of mutually interacting particles, such as, for example, two orbiting bodies.

In particular, consider the case of \(N\) particles with positions \(\mb{r}_1,...,\mb{r}_N\). It makes sense to consider the ``difference'' \(U\), which we call the \textbf{potential energy}, between the actual Lagrangian \(\mc{L}\) and the expression \ref{eqn:freelagrange}, which in general we call the \textbf{kinetic energy}. That is, we define
\begin{equation}
    U = \frac{1}{2}\sum_im_iv_i^2 - \mc{L} \iff \mc{L} = \frac{1}{2}\sum_im_iv_i^2 - U
\end{equation}
Continuing our analogy with the Newtonian formulation of mechanics, we call the term in expression \ref{eqn:freelagrange},
\begin{equation}
    T = \sum_im_iv_i^2
\end{equation}
the \textbf{kinetic energy} of the system, yielding the classical definition of the Lagrangian
\begin{equation}
    \mc{L} = T - U
\end{equation}
Of course, in this case, the Euler-Lagrange equations reduce directly to the usual Newtonian equation of motion
\begin{equation}
    \prt{L}{q} = -\prt{U}{\mb{r}_\alpha} = \drv{}{t}\left[\prt{mc{L}}{\dot{q}}\right] = \drv{}{t}\left[m_\alpha \mb{v}_\alpha\right] = m\alpha \mb{a}_\alpha
\end{equation}
We can again use the assumptions of classical mechanics to put some constraints on the form of \(U\) as a function of \(\mb{r}_i\), \(\mb{v}_i\), and \(t\).

In particular, time is still homogeneous, and hence as the Lagrangian cannot depend on \(t\), and \(T\) does not depend on \(t\), it follows that \(U\) must also be independent of \(t\).

\TODO{rewrite this paragraph...}

\TODO{this...}

\TODO{N = 2}

\TODO{consult treatment of this in Landau}

\section{External Fields and Constraints}

\TODO{clean up writing}

Using the Lagrangian formalism, it is easy to treat systems which cannot move completely freely. This is especially useful for mechanical systems like pulleys and rails. There are many approaches to introducing constraints into the formalism, but we will focus on the case of constraints of the form
\begin{equation}
    \mb{f}(q) = 0
\end{equation}
Of course, we assume \(\mb{f}: \reals^s \to \reals^k\) is ``sufficiently nice,'' in which case it's zero set will be a manifold of some dimension \(\leq s\), where \(q = (q_1,...,q_s)\). In particular, each component \(f_i\) of \(\mb{f}\) can be viewed as potentially reducing the number of coordinates by 1 (though, in some cases, this removal will be redundant or trivial, e.g. for \(f_i = 0\) or \(f_1 = f_2\)). In practice, this can be used to make seemingly very complex things very friendly.

As a running example demonstrating this principle, we will consider the pendulum.

\begin{figure}
    \input{figures/pendulum.tex}
    \caption{The pendulum system}
\end{figure}

\TODO{Disclaimer about Lagrangian used}

\TODO{Exposition}

\TODO{Small angle approximation}

\TODO{Diagram}

\end{document}