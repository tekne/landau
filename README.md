
# Course of Theoretical Physics

These are notes and solutions for the third edition of Lifshitz and Landau's excellent *Course of Theoretical Physics*. Currently, I hope to have a complete set of notes for Volume 1 (Mechanics) but may attempt other volumes if I find the time. These notes may be incomplete and/or incorrect, and as such, should be used at your own risk. 

These notes are available under the MIT license and are freely usable for any purpose (with attribution), including derivative works. I would highly appreciate any corrections, improvements, or suggestions, whether via issues, pull requests, or e-mail to jad.ghalayini@cs.ox.ac.uk. Let me know if these were helpful or if anything can be clarified!

You may also find my (unofficial) notes for the University of Toronto course [PHY354](https://gitlab.com/tekne/PHY354_Notes), as taught by Professor David Curtin, to be helpful; these covered most of chapters 1, 2, 3, 6, and 7 of Volume 1, with some interesting asides on the relationship between classical and quantum mechanics. That said, these are somewhat incomplete.
